const UserController = require('../controllers/user');
const CourseController = require('../controllers/course');

module.exports = {
	Query: {
		emailExists: (parent, args) => {
			return UserController.emailExists(args)
		},
		getAllCourses: (parent, args) => {
			return CourseController.getAll()
		},
		getCourse: (parent, args) => {
			return CourseController.get(args)
		},
		getUser: (parent, args, context) => {
			guardResolver(context.currentUser)
			return UserController.get({ userId: context.currentUser.id })
		}
	},
	Mutation: {
		register: (parent, args) => {
			return UserController.register(args)
		},
		login: (parent, args) => {
			return UserController.login(args)
		},
		addCourse: (parent, args, context) => {
			guardResolver(context.currentUser)
			return CourseController.add(args)
		},
		enroll: (parent, args, context) => {
			guardResolver(context.currentUser)
			return UserController.enroll({
				userId: context.currentUser.id,
				courseId: args.courseId
			})
		},
		updateCourse: (parent, args) => {
			return CourseController.update(args)
		},
		archive: (parent, args) => {
			return CourseController.archive(args)
		},

		// updateUserDetails: (parent, args, context) => {
		// 	guardResolver(context.currentUser)
		// 	return UserController.update({
		// 		userId: context.currentUser.id,
		// 		...args
		// 	})
		// },
		// updateUserPassword: (parent, args, context) => {
		// 	guardResolver(context.currentUser)
		// 	return UserController.changePassword({
		// 		userId: context.currentUser.id,
		// 		...args
		// 	})
		// },
		verifyGoogleTokenId: (parent, args, context) => {
			guardResolver(context.currentUser)
		}
	}
}

//checking
const guardResolver = (currentUser) => {
	if (currentUser === null){
		throw new Error('Authentication failure')
	}
}

//args - info
//context - contain info to be passed in graphql