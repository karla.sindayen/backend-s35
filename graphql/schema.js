const { gql } = require('apollo-server-express');

module.exports = gql`
	type Query {
		emailExists (email: String!): Boolean
		getAllCourses: [Course]
		getCourse(courseId: String!): Course
		getUser: User
	}

	type Mutation {

		register (
			firstName: String!,
			lastName: String!,
			email: String!,
			mobileNo: String!,
			password: String!
		): Boolean

		login (
			email: String!,
			password: String!
		): User

		addCourse (
			id: String!,
			name: String!,
			description: String!,
			price: Float!
		): Boolean

		enroll (
			courseId: String!
		): Boolean		

		verifyGoogleTokenId (
			tokenId: String!
		): User

		updateCourse (
			courseId: String!,
			name: String!,
			description: String!,
			price: Float!
		): Boolean

		archive (
			courseId: String!
		): Boolean
	}

	type User {
		id: String!,
		firstName: String!,
		lastName: String!,
		email: String!,
		mobileNo: String!,
		isAdmin: Boolean!,
		accessToken: String,
		enrollments: [UserEnrollments]
	}

	type UserEnrollments {
		id: String!,
		courseId: String!,
		enrolledOn: String!,
		status: String!
	}

	type Course {
		id: String!,
		name: String!,
		description: String!,
		price: Float!,
		isActive: Boolean!,
		createdOn: String!,
		enrollees: [CourseEnrollees]
	}

	type CourseEnrollees {
		id: String!,
		userId: String!,
		enrolledOn: String!
	}
`