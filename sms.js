const request = require('request')

module.exports.send = (mobileNumber, message) => {
    const shortCode = '21586422'
    const accessToken = 'K0bPyRAzVD8mvehxdCjrH2CuY73bPisFY29MIyK3i_g'
    const clientCorrelator = '910426'

    const options = {
        method: 'POST',
        url: 'https://devapi.globelabs.com.ph/smsmessaging/v1/outbound/' + shortCode + '/requests',
        qs: { 'access_token': accessToken },
        headers: { 'Content-Type': 'application/json' },
        body: {
            'outboundSMSMessageRequest': { 
                'clientCorrelator': clientCorrelator,
                'senderAddress': shortCode,
                'outboundSMSTextMessage': { 'message': message },
                'address': mobileNumber 
            } 
        },
        json: true 
    }

    request(options, (error, response, body) => {
        if (error) throw new Error(error)
        console.log(body)
    })
}